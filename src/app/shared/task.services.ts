import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";

export interface Tasks {
  id?:string,
  title: string,
  date?: string
}

interface CreateResponse {
  name: string
}

@Injectable({
  providedIn: 'root'
})

export class TaskServices {
  static url = 'https://angular-organizer-3d2b3-default-rtdb.firebaseio.com/tasks'

  constructor(private http: HttpClient) {
  }

  load(date: moment.Moment): Observable<Tasks[]> {
    return this.http.get<Tasks[]>(`${TaskServices.url}/${date.format('DD-MM-YYYY')}.json`)
      .pipe(
        map(tasks => {
          if (!tasks) {
            return []
          }
          return Object.keys(tasks).map((key: any) => ({
            ...tasks[key],
            id: key
          }))
        })
      )
  }

  create(task: Tasks): Observable<Tasks> {
    return this.http.post<CreateResponse>(`${TaskServices.url}/${task.date}.json`, task)
      .pipe(
        map(res => {
          return {...task, id: res.name}
        })
      )
  }

  remove(task: Tasks) {
    return this.http.delete<void>(`${TaskServices.url}/${task.date}/${task.id}.json`)
  }
}
